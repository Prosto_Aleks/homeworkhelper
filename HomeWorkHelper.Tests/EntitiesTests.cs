using HomeWorkHelper.Core.Entities;
using System;
using Xunit;

namespace HomeWorkHelper.Tests
{
    public class EntitiesTests
    {
        [Fact]
        public void UserTest()
        {
            User user1;
            Assert.Throws<ArgumentException>(() => user1 = new(-1, "as", 2));
            Assert.Throws<ArgumentException>(() => user1 = new(1, "", 2));
            Assert.Throws<ArgumentException>(() => user1 = new(1, "as", -2));

            User user2 = new(1, "�����", 1);
            User user2equals1 = null;
            Assert.NotEqual(user2, user2equals1);
            User user2equals2 = new(1, "����", 2);
            Assert.NotEqual(user2, user2equals2);
            Object obj = new();
            Assert.NotEqual(user2, obj);
            user1 = new(1, "�����", 1);
            Assert.Equal(user1, user2);
        }

        [Fact]
        public void GroupTest()
        {
            Group group1;
            Assert.Throws<ArgumentException>(() => group1 = new(-1, "as", 2, "s"));
            Assert.Throws<ArgumentException>(() => group1 = new(1, "", 2, "s"));
            Assert.Throws<ArgumentException>(() => group1 = new(1, "as", -2, "s"));
            Assert.Throws<ArgumentException>(() => group1 = new(1, "as", 2, ""));

            Group group2 = new(1, "as", 2, "s");
            Group group2equals1 = null;
            Assert.NotEqual(group2, group2equals1);
            Group group2equals2 = new(1, "adfg", 3, "a");
            Assert.NotEqual(group2, group2equals2);
            Object obj = new();
            Assert.NotEqual(group2, obj);
            group1 = new(1, "as", 2, "s");
            Assert.Equal(group1, group2);
        }

        [Fact]
        public void SubjectTest()
        {
            Subject sub1;
            Assert.Throws<ArgumentException>(() => sub1 = new(-1, "s", "s", "s", true, 1, "s", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "", "s", "s", true, 1, "s", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "s", "", "s", true, 1, "s", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "s", "s", "", true, 1, "s", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "s", "s", "s", true, -1, "s", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "s", "s", "s", true, 1, "", "s"));
            Assert.Throws<ArgumentException>(() => sub1 = new(1, "s", "s", "s", true, 1, "s", ""));

            Subject sub2 = new(1, "s", "s", "s", true, 1, "s", "s");
            Subject sub2equals1 = null;
            Assert.NotEqual(sub2, sub2equals1);
            Assert.False(sub2.Equals(sub2equals1));
            Subject sub2equals2 = new(2, "as", "as", "as", true, 2, "as", "as");
            Assert.NotEqual(sub2, sub2equals2);
            Assert.False(sub2.Equals(sub2equals2));
            Object obj = new();
            Assert.NotEqual(sub2, obj);
            Assert.False(sub2.Equals(obj));
            sub1 = new(1, "s", "s", "s", true, 1, "s", "s");
            Assert.Equal(sub1, sub2);
            Assert.True(sub1.Equals(sub2));
        }
    }
}