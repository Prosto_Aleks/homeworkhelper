﻿using HomeWorkHelper.Core.Abstractions.Databases;
using HomeWorkHelper.Core.Entities;
using HomeWorkHelper.Infrastructure.Database;
using System;
using System.Collections.Generic;
using Xunit;

namespace HomeWorkHelper.Tests.Repositories
{
    public class GroupRepository
    {
        IGroupRepository _groupRepository = new InMemoryGroupRepository();

        public GroupRepository()
        {
            _groupRepository.Create(new Group(1, "ПИН/б-21", 1, "Программная инженерия"));
            _groupRepository.Create(new Group(2, "ПИН/б-21", 2, "Программная инженерия"));
            _groupRepository.Create(new Group(3, "ИВТ/б-21", 1, "Информатика и вычислительная техника"));
        }

        [Fact]
        public void Should_return_all_groups()
        {
            List<Group> _testGroups = new();
            _testGroups.Add(new Group(1, "ПИН/б-21", 1, "Программная инженерия"));
            _testGroups.Add(new Group(2, "ПИН/б-21", 2, "Программная инженерия"));
            _testGroups.Add(new Group(3, "ИВТ/б-21", 1, "Информатика и вычислительная техника"));

            Assert.Equal(_groupRepository.GetAllGroups(), _testGroups);
        }

        [Fact]
        public void Should_return_group_by_id()
        {
            Assert.Equal(_groupRepository.GetGroupById(2), new Group(2, "ПИН/б-21", 2, "Программная инженерия"));
            Assert.Throws<ArgumentException>(() => _groupRepository.GetGroupById(-1));
        }

        [Fact]
        public void Should_return_group_by_name()
        {
            Assert.Equal(_groupRepository.GetGroupByName("ПИН/б-21-1"), new Group(1, "ПИН/б-21", 1, "Программная инженерия"));
            Assert.Throws<ArgumentException>(() => _groupRepository.GetGroupByName(""));
        }

        [Fact]
        public void Should_update_group()
        {
            var testGroup = _groupRepository.GetGroupById(2);
            _groupRepository.Update(new(2, "ПРН/б-21", 12, "Программная инженерность"));
            Assert.NotEqual(testGroup, _groupRepository.GetGroupById(2));

            Assert.Throws<ArgumentException>(() => _groupRepository.Update(new Group(1231, "", 1, "")));
        }
    }
}
