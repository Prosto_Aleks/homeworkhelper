﻿using HomeWorkHelper.Core;
using HomeWorkHelper.Core.Entities;
using HomeWorkHelper.Infrastructure.Database;
using System;
using System.Collections.Generic;
using Xunit;

namespace HomeWorkHelper.Tests.Repositories
{
    public class UserRepository
    {
        IUserRepository _userRepository = new InMemoryUserRepository();

        public UserRepository()
        {
            _userRepository.Add(new User(1, "A", 1));
            _userRepository.Add(new User(2, "B", 1));
            _userRepository.Add(new User(3, "C", 2));
            _userRepository.Add(new User(4, "D", 3));
        }

        [Fact]
        public void Should_return_all_users()
        {
            List<User> resultUsersList = new();
            resultUsersList.Add(new User(1, "A", 1));
            resultUsersList.Add(new User(2, "B", 1));
            resultUsersList.Add(new User(3, "C", 2));
            resultUsersList.Add(new User(4, "D", 3));

            Assert.Equal(_userRepository.GetAllUsers(), resultUsersList);
            Assert.Equal(_userRepository.GetAllUsers().Count, resultUsersList.Count);
        }

        [Fact]
        public void Should_return_user_by_id()
        {
            User userForTest = new(1, "A", 1);
            User userFromRepository = _userRepository.GetById(userForTest.Id);
            Assert.Equal(userForTest, userFromRepository);

            Assert.Throws<ArgumentNullException>(() => _userRepository.GetById(0));
        }
    }
}
