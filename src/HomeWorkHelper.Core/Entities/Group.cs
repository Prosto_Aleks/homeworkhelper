﻿namespace HomeWorkHelper.Core.Entities
{
    public class Group
    {
        private long _id;//Id группы в бд
        private string? _groupName;//Наименование группы (только первая часть), ПИН/б-21, ИВТ/б-20
        private int _subgroupId;//Номер подгруппы (1, 2, 3...), при сложении с groupName: "ПИН/б-21-1", "ИВТ/б-20-2" и т.д.
        private string? _specializationName;//Наименование направления/специализации, "Программная инженерия"

        public Group(long id, string groupName, int subgroupId, string specializationName)
        {
            Id = id;
            GroupName = groupName;
            SubgroupId = subgroupId;
            SpecializationName = specializationName;
        }

        public Group(Group group)
        {
            _id = group.Id;
            _groupName = group.GroupName;
            _subgroupId = group.SubgroupId;
            _specializationName = group.SpecializationName;
        }

        public long Id
        {
            get => _id;
            set
            {
                if (value < 0) throw new ArgumentException(nameof(Id));
                _id = value;
            }
        }

        public string? GroupName
        {
            get => _groupName;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(GroupName));
                _groupName = value;
            }
        }

        public int SubgroupId
        {
            get => _subgroupId;
            set
            {
                if (value < 0) throw new ArgumentException(nameof(SubgroupId));
                _subgroupId = value;
            }
        }

        public string? SpecializationName
        {
            get => _specializationName;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(_specializationName));
                _specializationName = value;
            }
        }

        public override bool Equals(object? obj)
        {
            if (this == obj)
                return true;

            if (obj == null)
                return false;

            if (obj is not Group)
                return false;

            Group user = (Group)obj;
            return Id == user.Id && GroupName == user.GroupName && SubgroupId == user.SubgroupId && SpecializationName == user.SpecializationName;
        }

        public override string ToString()
        {
            return $"Group {{\nId: {Id};\nName: {GroupName};\nSubgroupId: {SubgroupId};\nSpecialization: {SpecializationName};\n}}\n";
        }
    }
}
