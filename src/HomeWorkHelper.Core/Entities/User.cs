﻿namespace HomeWorkHelper.Core.Entities
{
    public class User
    {
        public long _id;
        public string _name;
        public int _groupId;

        public User(long id, string name, int groupId)
        {
            if (id < 0 || string.IsNullOrEmpty(name) || groupId < 0) throw new ArgumentException("Invalid constructor parameters");
            _id = id;
            _name = name;
            _groupId = groupId;
        }

        public User(User user)
        {
            _id = user._id;
            _name = user._name;
            _groupId = user._groupId;
        }

        public long Id
        {
            get => _id;
            set
            {
                if (value < 0) throw new ArgumentException(nameof(Id));
                _id = value;
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(Name));
                _name = value;
            }
        }
        public int GroupId
        {
            get => _groupId;
            set
            {
                if (value <= 0) throw new ArgumentException(nameof(_groupId));
                _groupId = value;
            }
        }

        public override bool Equals(object? obj)
        {
            if (this == obj)
                return true;

            if (obj == null)
                return false;

            if (obj is not User)
                return false;

            User user = (User)obj;
            return Id == user.Id && Name == user.Name && GroupId == user.GroupId;
        }

        public override string ToString()
        {
            return $"User{{\n Id: {Id};\nName: {Name};\nGroupId: {GroupId};\n}}\n";
        }
    }
}
