﻿namespace HomeWorkHelper.Core.Entities
{
    public class Subject
    {
        private int _id;// Id предмета в расписании
        private string _subjectName;// Название дисциплины
        private string _groupName;// Название группы у которой проходит дисциплина
        private string _dayOfTheWeek;// День недели, в который проходит дисциплина
        private bool _isEven;// Чётная ли неделя
        private int _lessonNumber;// Номер пары по расписанию
        private string _cabinet;// Название аудитории
        private string _activityType;// Тип пары(ПЗ, ЛЗ, Л)

        public Subject(int id, string subjectName, string groupName, string dayOfTheWeek, bool isEven, int lessonNumber,
                       string cabinet, string activityType)
        {
            if (id < 0 || string.IsNullOrEmpty(subjectName) || string.IsNullOrEmpty(groupName) || string.IsNullOrEmpty(dayOfTheWeek)
                || lessonNumber <= 0 || string.IsNullOrEmpty(cabinet) || string.IsNullOrEmpty(activityType)
                ) throw new ArgumentException("Constructor's argument is null or empty, or negative");
            _id = id;
            _subjectName = subjectName;
            _groupName = groupName;
            _dayOfTheWeek = dayOfTheWeek;
            _isEven = isEven;
            _lessonNumber = lessonNumber;
            _cabinet = cabinet;
            _activityType = activityType;
        }

        public Subject(Subject subject)
        {
            Id = subject.Id;
            SubjectName = subject.SubjectName;
            GroupName = subject.GroupName;
            DayOfTheWeek = subject.DayOfTheWeek;
            IsEven = subject.IsEven;
            LessonNumber = subject.LessonNumber;
            Cabinet = subject.Cabinet;
            ActivityType = subject.ActivityType;
        }

        public int Id
        {
            get => _id;
            set
            {
                if (value < 0) throw new ArgumentException(nameof(Id));
                _id = value;
            }
        }
        public string SubjectName
        {
            get => _subjectName;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(SubjectName));
                _subjectName = value;
            }
        }
        public string GroupName
        {
            get => _groupName;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(GroupName));
                _groupName = value;
            }
        }
        public string DayOfTheWeek
        {
            get => _dayOfTheWeek;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(DayOfTheWeek));
                _dayOfTheWeek = value;
            }
        }
        public bool IsEven { get => _isEven; set => _isEven = value; }
        public int LessonNumber
        {
            get => _lessonNumber;
            set
            {
                if (value <= 0) throw new ArgumentException(nameof(LessonNumber));
                _lessonNumber = value;
            }
        }
        public string Cabinet
        {
            get => _cabinet;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(Cabinet));
                _cabinet = value;
            }
        }
        public string ActivityType
        {
            get => _activityType;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentException(nameof(ActivityType));
                _activityType = value;
            }
        }

        public override bool Equals(object? obj)
        {
            if (this == obj)
                return true;

            if (obj == null)
                return false;

            if (obj is not Subject)
                return false;

            Subject subject = (Subject)obj;
            return Id == subject.Id && SubjectName == subject.SubjectName && GroupName == subject.GroupName &&
                DayOfTheWeek == subject.DayOfTheWeek && IsEven == subject.IsEven && LessonNumber == subject.LessonNumber &&
                Cabinet == subject.Cabinet && ActivityType == subject.ActivityType;
        }

        public override string ToString()
        {
            return $"Subject:\n\tId: {Id};\n\tSubjectName: {SubjectName};\n\tGroupName: {GroupName};\n\tDay of the week: {DayOfTheWeek};\n\t" +
                $"Is Even: {IsEven};\n\tLesson number: {LessonNumber};\n\tCabinet: {Cabinet};\n\tActivity type: {ActivityType}\n";
        }
    }
}
