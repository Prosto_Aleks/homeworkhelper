﻿using HomeWorkHelper.Core.Entities;

namespace HomeWorkHelper.Core.Abstractions.Databases
{
    public interface IGroupRepository
    {
        public List<Group> GetAllGroups();
        public Group GetGroupById(long id);
        public Group GetGroupByName(string groupName);
        public void Update(Group group);
        public void Delete(Group group);
        public void Create(Group group);
    }
}
