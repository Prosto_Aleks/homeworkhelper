﻿using HomeWorkHelper.Core.Entities;

namespace HomeWorkHelper.Core
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        User GetById(long id);
        void Add(User user);
        void Update(User user);
        void Delete(long id);
    }
}
