﻿using HomeWorkHelper.Core;
using HomeWorkHelper.Core.Entities;

namespace HomeWorkHelper.Infrastructure.Database
{
    public class InMemoryUserRepository : IUserRepository
    {
        private readonly List<User> _users = new();

        public void Add(User user)
        {
            _users.Add(user);
        }

        public void Delete(long id)
        {
            User user = _users.Find(x => x.Id == id);
            if (user == null) throw new ArgumentNullException(nameof(user));
            _users.Remove(user);
        }

        public List<User> GetAllUsers()
        {
            return _users;
        }

        public User GetById(long id)
        {
            User user = _users.Find(x => id == x.Id);
            if (user == null) throw new ArgumentNullException(nameof(user));
            return user;
        }

        public void Update(User user)
        {
            int index = _users.IndexOf(user);
            _users[index].Id = user.Id;
            _users[index].Name = user.Name;
            _users[index].GroupId = user.GroupId;
        }
    }
}