﻿using HomeWorkHelper.Core.Abstractions.Databases;
using HomeWorkHelper.Core.Entities;

namespace HomeWorkHelper.Infrastructure.Database
{
    public class InMemoryGroupRepository : IGroupRepository
    {
        List<Group> _groups = new();

        public void Create(Group group)
        {
            _groups.Add(group);
        }

        public void Delete(Group group)
        {
            _groups.Remove(group);
        }

        public List<Group> GetAllGroups()
        {
            return _groups;
        }

        public Group GetGroupById(long id)
        {
            Group? group = _groups.Find(x => x.Id == id);
            if (group == null) throw new ArgumentException(nameof(group));
            return group;
        }

        public Group GetGroupByName(string groupName)
        {
            Group? group = _groups.Find(x => (x.GroupName + "-" + x.SubgroupId) == groupName);
            if (group == null) throw new ArgumentException(nameof(group));
            return group;
        }

        public void Update(Group group)
        {
            int index = _groups.IndexOf(_groups.Find(x => x.Id == group.Id));
            index--;
            if (index == -1) throw new ArgumentException();
            _groups[index].Id = group.Id;
            _groups[index].GroupName = group.GroupName;
            _groups[index].SubgroupId = group.SubgroupId;
            _groups[index].SpecializationName = group.SpecializationName;
        }
    }
}
